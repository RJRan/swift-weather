//
//  ViewController.swift
//  SwiftWeather
//
//  Created by Robert Randell on 23/09/2014.
//  Copyright (c) 2014 The APS Group. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var precipitationLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var refreshActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var locationScroller: UIScrollView!
    
    private let apiKey = "35bcfad69cf6113bff0fea81926e7d6a"
    
    let locationManager = CLLocationManager()
    
    let locations = ["Current", "London", "Manchester", "New York", "Rome", "Paris"]
    
    var latitude: Double = 0
    var longitude: Double = 0

    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        refreshActivityIndicator.hidden = true
        
        locationScroller.delegate = self
        locationScroller.bounces = false
        locationScroller.scrollEnabled = true
        
        setupLocations(locations)
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.startUpdatingLocation()
        
    }
    
    func setupLocations(locations: NSArray) {
        
        var x: CGFloat = 0.0
        var y: CGFloat = 0.0
        
        let buttonContainerWidth: CGFloat = 120.0
        let buttonContainerHeight: CGFloat = 60.0
        
        let buttonWidth: CGFloat = 100.0
        let buttonHeight: CGFloat = 40.0
        let buttonMargin: CGFloat = 10
        
        locationScroller.contentSize = CGSizeMake((buttonContainerWidth * CGFloat(locations.count)), buttonHeight)
        
        for var i = 0; i < locations.count; i++ {
            
            let locationBtnView = UIView()
            locationBtnView.frame = CGRectMake(x, y, buttonContainerWidth, buttonContainerHeight)
            locationBtnView.backgroundColor = UIColor.clearColor()
            
            let locationButton = UIButton.buttonWithType(UIButtonType.System) as UIButton
            locationButton.frame = CGRectMake(buttonMargin, buttonMargin, buttonWidth, buttonHeight)
            locationButton.backgroundColor = UIColor.clearColor()
            locationButton.tintColor = UIColor.whiteColor()
            locationButton.tag = i
            locationButton.setTitle(self.locations[i], forState: UIControlState.Normal)
            locationButton.addTarget(self, action: "setNewLocation:", forControlEvents: UIControlEvents.TouchUpInside)
            
            locationBtnView.addSubview(locationButton)
            locationScroller.addSubview(locationBtnView)
            
            x = x + buttonContainerWidth
            
        }
    }
    
    func setNewLocation(sender:UIButton) {
        
        var gpsCoords = [0.0000, 0.0000]
        
        if let location = sender.titleLabel?.text {
            
            self.locationLabel.text = location
            
            switch location {
                case "London" :
                    gpsCoords = [51.507197, -0.127898]
                case "Manchester" :
                    gpsCoords = [53.479180, -2.249062]
                case "New York" :
                    gpsCoords = [40.714121, -74.003109]
                case "Rome" :
                    gpsCoords = [41.897598, 12.498340]
                case "Paris" :
                    gpsCoords = [48.852929, 2.350236]
                default :
                    gpsCoords = [latitude, longitude]
                    
                    locationManager.startUpdatingLocation()
            }
        }
        else {
            
        }
        getCurrentWeatherData(gpsCoords[0], longitude: gpsCoords[1])
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        
        var locationArray = locations as NSArray
        var locationObj = locationArray.lastObject as CLLocation
        
        latitude = locationObj.coordinate.latitude as Double
        longitude = locationObj.coordinate.longitude as Double
        
        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: {(placemarks, error) -> Void in
            
            if (error != nil) {
                println("Reverse geocoder failed with error " + error.localizedDescription)
            }
            else {
                if (placemarks.count > 0) {
                    let placemark = placemarks[0] as CLPlacemark
                    self.displayLocationInfo(placemark)
                }
                else {
                    println("Problem with the data received from Geocoder")
                }
            }
        })
        
        getCurrentWeatherData(latitude, longitude: longitude)
        
        locationManager.stopUpdatingLocation()

    }
    
    func displayLocationInfo(placemark: CLPlacemark) {
        
        locationLabel.text = "\(placemark.locality)"
    }

    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        
        println("Location Error: " + error.localizedDescription)
    }
    
    func getCurrentWeatherData(latitude: Double, longitude: Double) -> Void {
        
        let baseURL = NSURL(string: "https://api.forecast.io/forecast/\(apiKey)/")
        let forecastURL = NSURL(string: "\(latitude),\(longitude)", relativeToURL:baseURL)
        
        let sharedSession = NSURLSession.sharedSession()
        let downloadTask: NSURLSessionDownloadTask = sharedSession.downloadTaskWithURL(forecastURL, completionHandler: {
            (location: NSURL!, response: NSURLResponse!, error: NSError!) -> Void in
            
            if (error == nil) {
                let dataObject = NSData(contentsOfURL: location)
                let weatherDictionary: NSDictionary = NSJSONSerialization.JSONObjectWithData(dataObject, options: nil, error: nil) as NSDictionary
                let currentWeather = Current(weatherDictionary: weatherDictionary)
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    self.temperatureLabel.text = "\(currentWeather.temperature!)"
                    self.iconView.image = currentWeather.icon!
                    self.currentTimeLabel.text = "At \(currentWeather.currentTime!) it is"
                    self.humidityLabel.text = "\(currentWeather.humidity)"
                    self.precipitationLabel.text = "\(currentWeather.precipProbability)"
                    self.summaryLabel.text = "\(currentWeather.summary)"
                    //self.locationLabel.text = "\(currentWeather.location)"
                    
                    self.refreshActivityIndicator.stopAnimating()
                    self.refreshActivityIndicator.hidden = true
                    self.refreshButton.hidden = false
                })
            }
            else {
                
                let networkIssueController = UIAlertController(title: "Error", message: "Unable to load data. Connectivity error!", preferredStyle: .Alert)
                
                let okButton = UIAlertAction(title: "OK", style: .Default, handler: nil)
                let cancelButton = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
                networkIssueController.addAction(okButton)
                networkIssueController.addAction(cancelButton)
                
                self.presentViewController(networkIssueController, animated: true, completion: nil)
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.refreshActivityIndicator.stopAnimating()
                    self.refreshActivityIndicator.hidden = true
                    self.refreshButton.hidden = false
                })
            }
        })
        downloadTask.resume()
    }

    @IBAction func refresh() {
        
        refreshActivityIndicator.hidden = false
        refreshButton.hidden = true
        refreshActivityIndicator.startAnimating()
        locationManager.startUpdatingLocation()
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
